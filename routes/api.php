<?php

use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/* Check Token */
Route::middleware('jwt.auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/* Register Token */
Route::post('user', 'AuthController@authenticate');
    //$2y$10$OUMORtYdsdPTeloNScsjZu12twskWhCCWAOkPXtPDnZdCJqX5Jwoi
Route::post('user/create', 'UserController@create');
Route::get('/', 'UserController@store');