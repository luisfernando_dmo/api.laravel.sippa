<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $allrotes = null;
    foreach (Route::getRoutes() as $route) {
        $allrotes[] = $route->uri();
    }
    return view('welcome')->with('data', $allrotes);
});