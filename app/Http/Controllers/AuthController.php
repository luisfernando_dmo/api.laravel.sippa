<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use JWTAuth;

class AuthController extends Controller
{

    public function authenticate (Request $request){
   
        $credentials = $request->only('email', 'password');
        $user = User::where('email', $credentials['email'])->first();

        if(!$user) {
            return response()->json([
                'error' => 'Invalid credentials'
            ], 401);
        }
        /* Validate Password */
        if (!Hash::check($credentials['password'], $user->password)){
            /* TODO: MD5 JAVA */
            return response()->json([
                'error' => 'Invalid credentials'
            ], 401);
        }

        $token = JWTAuth::fromUser($user);
        $objectToken = JWTAuth::setToken($token);
        $expiration = JWTAuth::decode($objectToken->getToken())->get('exp');

        $user->remember_token = $token;
        $user->save();

        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => $expiration
        ]);
    }
}
