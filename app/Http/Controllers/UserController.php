<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class UserController extends Controller
{
    public function create(Request $request){
        return redirect()->action('UserController@store', $request->toArray());
    }

    public function store(Request $request){

            $password = Hash::make($request->password);
            $data = [
                'name' => $request->name,
                'email' => $request->email,
                'password' => $password
            ];
            try{
                User::create($data);
                return response()->json([
                    'success' => 'User created successfully'
                ], 200);
                return "User created successfully";
            }catch (ValidationException $e){
                return response()->json([
                    'error' => $e->getMessage()
                ], 500);
            }
    }
}